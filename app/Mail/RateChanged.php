<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class RateChanged extends Mailable
{
    use Queueable, SerializesModels;

    public $user;
    public $currency;
    public $oldRate;

    /**
     * Create a new message instance.
     *
     * @param $user
     * @param $currency
     * @param $oldRate
     */
    public function __construct($user, $currency, $oldRate)
    {
        $this->user = $user;
        $this->currency = $currency;
        $this->oldRate = $oldRate;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->subject('Message from Crypto Market Service!')
                    ->view('emails.notify');
    }
}
