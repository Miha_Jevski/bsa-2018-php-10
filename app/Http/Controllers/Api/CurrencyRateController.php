<?php

namespace App\Http\Controllers\Api;

use App\Entity\Currency;
use App\Jobs\SendRateChangedEmail;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class CurrencyRateController extends Controller
{
    public function update(Request $request, $id)
    {
        $currency = Currency::find($id);

        if ($currency) {
            $oldRate = $currency->rate;

            $currency->update($request->only('rate'));

            $users = User::notAdmin()->get();
            foreach ($users as $user){
                $job = (new SendRateChangedEmail($user, $currency, $oldRate))
                            ->onQueue('notification');
                dispatch($job);
            }

            return response()->json([]);
        }

        return response()->json([], 404);

    }
}
