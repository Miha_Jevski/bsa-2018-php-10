<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>
<body>
<div class="container">
    <h2>Dear {{$user->name}},</h2>

    <p>{{$currency->name}} exchange rate has been changed from {{$oldRate}} to {{$currency->rate}}!</p>

    <h3>Thanks,<br>Crypto Market Service!</h3>
</div>
</body>
</html>