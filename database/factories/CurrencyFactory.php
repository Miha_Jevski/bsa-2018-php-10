<?php

use Faker\Generator as Faker;

$factory->define(App\Entity\Currency::class, function (Faker $faker) {
    return [
        'name' => $faker->regexify('[A-Z0-9]{5}'),
        'rate' => $faker->randomFloat(2, 50, 100),
    ];
});
